#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <list>
#include <vector>
#include <math.h>
#include <limits>
#include <fstream>

// half edge structure
typedef struct HE {
    struct HE * brother; // the HE in the opposite direction.
    struct HEF * adj_face; // the face that this half edge originates from
    struct HE * next; // the HE that is 'out' of 'vertex' and has the same 'adj_face'
    struct HEV * vertex; // the vertex this HE points to
} HE;

//half edge face
typedef struct HEF {
    int a,b,c; // useful for helping the initial loading process.
    struct HE * adj; // one of the three HEs that surround a face, picked randomly
    int oriented; // also useful for the loading. marks if this face was oriented or not.
} HEF;

//half edge vertex
typedef struct HEV {
    float x;
    float y;
    float z;
    struct HE * out; // one of the HEs coming out of this vertex.
} HEV;

// vector. Feel free to use or ignore it.
typedef struct Tvec {
    float x,y,z;

} Tvec;
